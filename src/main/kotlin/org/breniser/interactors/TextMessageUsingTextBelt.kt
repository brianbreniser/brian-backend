package org.breniser.interactors

import com.fasterxml.jackson.databind.ObjectMapper
import org.breniser.interactors.features.CanSendTextMessage
import org.breniser.interactors.features.TextMessageResponse
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class TextMessageUsingTextBelt: CanSendTextMessage {
    private val apiKey = "apiKey"

    override fun sendMessage(message: String, phoneNumber: String): TextMessageResponse {

        val postArguments = mapOf(
            "message" to message,
            "phone" to phoneNumber,
            "key" to apiKey
        )

        val client = HttpClient.newHttpClient()

        val request = HttpRequest.newBuilder()
            .uri(URI.create("https://textbelt.com/text"))
            .POST(HttpRequest.BodyPublishers.ofString("{\"phone\":\"5035040031\"}"))
            .build()

        println(request)
        println(postArguments)
        println(request.headers())
        println(request.bodyPublisher())

        val response = client.send(request, HttpResponse.BodyHandlers.ofString())

        println(response)
        println(response.body())

        return TextMessageResponse(
            true,
            message,
            response.body()

        )
    }
}

package org.breniser.interactors.features

class TextMessageResponse(
    wasSuccessful: Boolean = false,
    message: String = "",
    otherInfo: String = ""
) {
    var wasSuccessful: Boolean = wasSuccessful
    var message: String = message
    var otherInfo: String = otherInfo
}

interface CanSendTextMessage {
    fun sendMessage(message: String, phoneNumber: String): TextMessageResponse
}
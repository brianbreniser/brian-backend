package org.breniser.interactors

import org.breniser.interactors.features.CanSendTextMessage
import org.breniser.interactors.features.TextMessageResponse

class FakeTextMessage: CanSendTextMessage {
    override fun sendMessage(message: String, phoneNumber: String): TextMessageResponse {
        println("A fake text message was sent to phone: $phoneNumber with the message: $message")

        return TextMessageResponse(
            wasSuccessful = true,
            message = "Fake sent successfully",
            otherInfo = "This was a fake text message"
        )
    }
}
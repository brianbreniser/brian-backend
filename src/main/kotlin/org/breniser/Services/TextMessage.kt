package org.breniser.Services

import org.breniser.interactors.features.CanSendTextMessage
import org.breniser.interactors.features.TextMessageResponse

class TextMessage(sendTextMessage: CanSendTextMessage) {

    var sendMessage: CanSendTextMessage = sendTextMessage

    fun sendAText(message: String, phoneNumber: String): TextMessageResponse {
        return this.sendMessage.sendMessage(message, phoneNumber)
    }
}
package org.breniser.incomingAPI

import org.breniser.Services.Greeting
import org.jboss.resteasy.annotations.jaxrs.PathParam
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/greet")
class GreetingResource {
    private val greetingService: Greeting = Greeting()

    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    fun hello() = "Hello RESTEasy"

    @GET
    @Path("/bye")
    @Produces(MediaType.TEXT_PLAIN)
    fun goodbye() = "Bye REST"

    @GET
    @Path("/meet/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    fun meet(@PathParam name: String) = greetingService.MeetPerson(name)
}
package org.breniser.incomingAPI

import org.breniser.interactors.FakeTextMessage
import org.breniser.Services.TextMessage
import org.breniser.interactors.TextMessageUsingTextBelt
import org.jboss.resteasy.annotations.jaxrs.PathParam
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/tools")
class TextApi {
    private val textService = TextMessage(TextMessageUsingTextBelt())

    @GET
    @Path("/text/{number}/{message}")
    @Produces(MediaType.TEXT_PLAIN)
    fun sendAText(
        @PathParam number: String,
        @PathParam message: String
    ): String {
        val response = textService.sendAText(message, number)
        return "A message $message was sent to phone number: $number and has this object as a response: success: ${response.wasSuccessful}, message: ${response.message}, and extra info: ${response.otherInfo}"
    }
}
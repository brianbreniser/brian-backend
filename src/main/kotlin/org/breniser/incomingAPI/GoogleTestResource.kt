package org.breniser.incomingAPI

import org.breniser.interactors.Google
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/google")
class GoogleTestResource {
    private val google = Google()

    @GET
    @Path("/test")
    @Produces(MediaType.TEXT_PLAIN)
    fun google() = google.tryGoogleCom()
}

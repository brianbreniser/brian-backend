package org.breniser.Services

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class GreetingTest {
    private lateinit var service: Greeting

    @BeforeEach
    fun setUp() {
        this.service = Greeting()
    }

    @AfterEach
    fun tearDown() {
    }

    @Test
    fun `MeetPerson returns a well formatted string`() {
        var result = service.MeetPerson("aperson")
        assertEquals("Hello aperson", result)
    }
}